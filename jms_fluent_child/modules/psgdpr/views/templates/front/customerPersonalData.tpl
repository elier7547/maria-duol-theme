{*
* 2007-2018 PrestaShop
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2018 PrestaShop SA
* @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
* International Registered Trademark & Property of PrestaShop SA
*}
{extends file='customer/page.tpl'}

{block name='page_title'}
    {l s='GDPR - Personal data' mod='psgdpr'}
{/block}

{block name='page_content'}
<div class="container">
    <section class="page_content">
        <div class="col-xs-12 psgdprinfo17">
            <h2>{l s='Acceso a mis datos' mod='psgdpr'}</h2>
            <p>{l s='En cualquier momento, tiene derecho a recuperar los datos que ha proporcionado a nuestro sitio. Haga clic en "Obtener mis datos" para descargar automáticamente una copia de sus datos personales en un archivo pdf o csv.' mod='psgdpr'}.</p>
            <a id="exportDataToCsv" class="btn btn-primary psgdprgetdatabtn17" target="_blank" href="{$psgdpr_csv_controller|escape:'htmlall':'UTF-8'}">{l s='OBTENER MIS DATOS A CSV' mod='psgdpr'}</a>
            <a id="exportDataToPdf" class="btn btn-primary psgdprgetdatabtn17" target="_blank" href="{$psgdpr_pdf_controller|escape:'htmlall':'UTF-8'}">{l s='OBTENER MIS DATOS A PDF' mod='psgdpr'}</a>
        </div>
        <div class="col-xs-12 psgdprinfo17">
            <h2>{l s='Rectificación y solicitudes de borrado' mod='psgdpr'}</h2>
            <p>{l s='Tiene derecho a modificar toda la información personal que se encuentra en la página "Mi cuenta". Para cualquier otra solicitud que pueda tener con respecto a la rectificación y / o eliminación de sus datos personales, contáctenos a través de nuestra' mod='psgdpr'} <a href="{$psgdpr_contactUrl|escape:'htmlall':'UTF-8'}">{l s='pagina de contacto' mod='psgdpr'}</a>. {l s='Revisaremos su solicitud y le responderemos lo antes posible.' mod='psgdpr'}.</p>
        </div>
    </section>
</div>
{literal}
<script type="text/javascript">
    var psgdpr_front_controller = "{/literal}{$psgdpr_front_controller|escape:'htmlall':'UTF-8'}{literal}";
    var psgdpr_id_customer = "{/literal}{$psgdpr_front_controller|escape:'htmlall':'UTF-8'}{literal}";
    var psgdpr_ps_version = "{/literal}{$psgdpr_ps_version|escape:'htmlall':'UTF-8'}{literal}";
</script>
{/literal}
{/block}
