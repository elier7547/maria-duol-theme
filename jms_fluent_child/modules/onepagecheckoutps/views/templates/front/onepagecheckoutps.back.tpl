{*
    * We offer the best and most useful modules PrestaShop and modifications for your online store.
    *
    * We are experts and professionals in PrestaShop
    *
    * @author    PresTeamShop.com <support@presteamshop.com>
    * @copyright 2011-2020 PresTeamShop
    * @license   see file: LICENSE.txt
    * @category  PrestaShop
    * @category  Module
*}<!doctype html>
<html lang="{$language.iso_code|escape:'htmlall':'UTF-8'}">
    <head>
        {block name='head'}
            {include file='_partials/head.tpl'}
        {/block}
    </head>

    <body id="{$page.page_name}" class="{$page.body_classes|classnames}">
        {block name='hook_after_body_opening_tag'}
            {hook h='displayAfterBodyOpeningTag'}
        {/block}
        {block name='notifications'}
            {include file='_partials/notifications.tpl'}
        {/block}
        

        <section id="wrapper">
            <div class="container">
            {block name='content'}
                <section id="main">
                    {$onepagecheckoutps->includeTpl('theme.tpl', ['CONFIGS' => $OnePageCheckoutPS.CONFIGS]) nofilter}

                    <div id="onepagecheckoutps" class="js-current-step {if $register_customer}rc{/if}">
                        <input type="hidden" id="logged" value="{$customer.is_logged|intval}" />

                        <div class="loading_big">
                            <div class="loader">
                                <div class="dot"></div>
                                <div class="dot"></div>
                                <div class="dot"></div>
                                <div class="dot"></div>
                                <div class="dot"></div>
                            </div>
                        </div>

                        {hook h='emailVerificationOPC'}

                        {if !$register_customer}
                            <div id="onepagecheckoutps_header" class="col-md-7">
                                <div class="row">
                                    <div id="div_onepagecheckoutps_info" class="{if $customer.is_logged and !$customer.is_guest}col-md-8{/if} col-sm-12 col-xs-12 col-12">
                                        <div class="imagen">
                                            <a href="{$urls.base_url}" title="María Doul"><img src="{$shop.logo}" title="María Doul" alt="María Doul" class="logo-checkout" /></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/if}

                        <div class="row">
                            {$onepagecheckoutps->includeTpl('custom_html/header.tpl', []) nofilter}
                        </div>

                        <div id="onepagecheckoutps_contenedor" class="col-xs-12 col-12">
                            <div id="onepagecheckoutps_forms" class="hidden"></div>
                            <div id="opc_temporal" class="hidden"></div>
                            <div class="row contenedorpasos">
                            
                                {foreach from=$position_steps item=column}
                                            {foreach from=$column.rows item=row}
                                                {if $row.name_step == "customer"}
                                                    <div class="col-md-6 pasos">
                                                {/if}
                                                
                                                {if $row.name_step == "review"}


                                                    {if !$customer.is_logged or ($customer.is_logged and $customer.is_guest)}

                                                        <div class="hidden contenedorlogin col-md-6">
                                                            {if $opc_social_networks}
                                                                <section id="opc_social_networks">
                                                                    {foreach from=$opc_social_networks key='name' item='network'}
                                                                        {if $network->client_id neq '' && $network->client_secret neq '' && $network->enable > 0}
                                                                            <button type="button" class="btn btn-sm btn-{$name|escape:'htmlall':'UTF-8'}" onclick="Fronted.openWindow('{$link->getModuleLink('onepagecheckoutps', 'login', ['sv' => $network->network])}', true)">
                                                                                {if $network->name_network eq 'Google'}
                                                                                    <img src="{$OnePageCheckoutPS.ONEPAGECHECKOUTPS_IMG|escape:'htmlall':'UTF-8'}social/btn_google.png" alt="google">
                                                                                {elseif $network->name_network eq 'Biocryptology'}
                                                                                    <img src="{$OnePageCheckoutPS.ONEPAGECHECKOUTPS_IMG|escape:'html':'UTF-8'}social/btn_biocryptology.png" alt="biocryptology">
                                                                                {else}
                                                                                        <i class="fa-pts fa-pts-1x fa-pts-{$network->class_icon|escape:'htmlall':'UTF-8'}"></i>
                                                                                {/if}
                                                                                {$network->name_network}
                                                                            </button>
                                                                        {/if}
                                                                    {/foreach}
                                                                </section>
                                                                <br/>
                                                            {/if}

                                                            
                                                            <h5 class="titulo_iniciar">
                                                                {l s='Iniciar sesión' mod='onepagecheckoutps'}
                                                            </h5>
                                                            <form id="form_login" autocomplete="off">
                                                                <div class="row">
                                                                    <div class="form-group col-xs-12 col-12  clear clearfix">
                                                                        <input
                                                                            id="txt_login_email"
                                                                            class="form-control"
                                                                            type="text"
                                                                            placeholder="{l s='E-mail' mod='onepagecheckoutps'}"
                                                                            data-validation="isEmail"
                                                                        />
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group col-xs-12 col-12  clear clearfix">
                                                                        <input
                                                                            id="txt_login_password"
                                                                            class="form-control"
                                                                            type="password"
                                                                            placeholder="{l s='Password' mod='onepagecheckoutps'}"
                                                                            data-validation="length"
                                                                            data-validation-length="min5"
                                                                        />
                                                                    </div>
                                                                </div>
                                                                <div class="alert alert-warning  pts-nopadding hidden"></div>
                                                                <div class="login_footer">
                                                                    <a href="{$urls.pages.password|escape:'htmlall':'UTF-8'}">{l s='¿Olvidaste tu costraseña?' mod='onepagecheckoutps'}</a>
                                                                    
                                                                    <button type="button" id="btn_login" class="btn btn-info float-right">
                                                                        {l s='Iniciar sesión' mod='onepagecheckoutps'}
                                                                    </button>
                                                                </div>
                                                                <div class="row_invitada">
                                                                    <a href="#" class="invitada">¿Quieres realizar la compra como invitada sin tener que registrarte?</a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    {/if}

                                                    
                                                    <div class="col-md-5 col-md-offset-1">
                                                {/if}
                                                {$onepagecheckoutps->includeTpl('steps/'|cat:$row.name_step|cat:'.tpl', [register_customer => $register_customer, classes => $row.classes, 'CONFIGS' => $OnePageCheckoutPS.CONFIGS, 'OnePageCheckoutPS' => $OnePageCheckoutPS]) nofilter}

                                                
                                                {if $row.name_step == "payment" || $row.name_step == "review"}
                                                    </div>
                                                {/if}
                                            {/foreach}
                                {/foreach}
                            </div>
                        </div>

                        <div class="row">
                            {$onepagecheckoutps->includeTpl('custom_html/footer.tpl', []) nofilter}
                        </div>

                        <div id="payment-confirmation" class="hidden"><div class="ps-shown-by-js"><button class="button btn-primary" type="submit"></button></div></div>

                        <div class="clear clearfix"></div>
                    </div>
                    
                    <div id="onepagecheckoutps_footer" class="col-md-7">
                        <a href="/politica-de-devoluciones" target="_blank" title="Política de devoluciones">Política de devoluciones</a> <a target="_blank" href="/politica-de-privacidad" title="Política de privacidad">Política de privacidad</a> <a target="_blank" href="/terminos-y-condiciones" title="Términos y condiciones">Términos y condiciones</a>
                    <div>
                </section>
            {/block}
            </div>
            
            <div class="row fondo">
                <div class="col-md-7"></div>
                <div class="col-md-5"></div>
            </div>
        </section>
        {block name='javascript_bottom'}
            {include file="_partials/javascript.tpl" javascript=$javascript.bottom}
        {/block}

<script src="{$urls.theme_assets}js/jquery.steps.min.js"></script>
<script>
$(".pasos").steps( {
    headerTag: "h6",
    bodyTag: ".paso",
    transitionEffect: "slideLeft",
    autoFocus: true
} );
</script>
        {block name='hook_before_body_closing_tag'}
            {hook h='displayBeforeBodyClosingTag'}
        {/block}
    </body>
</html>