{*
    * We offer the best and most useful modules PrestaShop and modifications for your online store.
    *
    * We are experts and professionals in PrestaShop
    *
    * @author    PresTeamShop.com <support@presteamshop.com>
    * @copyright 2011-2020 PresTeamShop
    * @license   see file: LICENSE.txt
    * @category  PrestaShop
    * @category  Module
*}

<div class="row {if isset($productLast) and $productLast && (not isset($ignoreProductLast) or !$ignoreProductLast)}last_item{elseif isset($productFirst) and $productFirst}first_item{/if} {if isset($customizedDatas.$productId.$productAttributeId) AND $quantityDisplayed == 0}alternate_item{/if} cart_item address_{$product.id_address_delivery|intval}"
     id="product_{$product.id_product|intval}_{$product.id_product_attribute|intval}_0_{$product.id_address_delivery|intval}{if !empty($product.gift)}_gift{/if}">
    <div class="col-md-3 col-xs-3 col-sm-3 col-3 text-md-center image_product">
        {*if isset($product.productmega)}
            {assign var="path_img" value="{$smarty.const._PS_MODULE_DIR_}/megaproduct/images/cart/{$mega.id_megacart|intval}.jpg"}
        {/if*}
        {*if isset($product.productmega) and file_exists($path_img)}
            <a href="{$product.url|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'htmlall':'UTF-8'}">
                <img class="img-fluid media-object" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}megaproduct/images/cart/{$mega.id_megacart|intval}.jpg" alt="{$product.name|escape:'htmlall':'UTF-8'}"/>
            </a>

            {if $CONFIGS.OPC_SHOW_ZOOM_IMAGE_PRODUCT}
                <div class="image_zoom">
                    <img class="media-object" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}megaproduct/images/cart/{$mega.id_megacart|intval}.jpg" alt="{$product.name}"/>
                </div>
            {/if}
        {else*}
            <a href="{$product.url|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'htmlall':'UTF-8'}">
                <img class="img-fluid media-object" src="{$product.cover.bySize.small_default.url|escape:'htmlall':'UTF-8'}" alt="{$product.name|escape:'htmlall':'UTF-8'}"/>
            </a>

            {*if $CONFIGS.OPC_SHOW_ZOOM_IMAGE_PRODUCT}
                <div class="image_zoom">
                    <img class="media-object" src="{$product.cover.medium.url|escape:'htmlall':'UTF-8'}" alt="{$product.name|escape:'htmlall':'UTF-8'}"/>
                </div>
            {/if*}
        {*/if*}
    </div>
    <div class="col-md-6 col-xs-9 col-sm-9 col-9 cart_description">
        <p class="s_title_block">
            {if !$CONFIGS.OPC_REMOVE_LINK_PRODUCTS}
                <a href="{$product.url|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'htmlall':'UTF-8'}">
            {/if}
                {$product.name|escape:'htmlall':'UTF-8'}
                {if $product.reference and $CONFIGS.OPC_SHOW_REFERENCE}
                    <span class="product_reference">
                        ({l s='Ref.' mod='onepagecheckoutps'}&nbsp;{$product.reference|escape:'htmlall':'UTF-8'})
                    </span>
                {/if}
            {if !$CONFIGS.OPC_REMOVE_LINK_PRODUCTS}
                </a>
            {/if}
        </p>
        {if isset($product.attributes) && $product.attributes}
            <span class="product_attributes">
                {foreach from=$product.attributes key="attribute" item="value"}
                    <div class="product-line-info">
                        <span class="">{$attribute|escape:'htmlall':'UTF-8'}:</span>
                        <span class="">{$value|escape:'htmlall':'UTF-8'}</span>
                    </div>
                {/foreach}
            </span>
        {/if}

        {if !empty($product.customizations)}
            <span>
                <strong>{l s='Customization' mod='onepagecheckoutps'}:</strong>
                {foreach $product.customizations as $customization}
                    {foreach $customization.fields as $key => $field}
                        {if $key > 0}, {/if}{$field.label nofilter}: {$field.text nofilter}
                    {/foreach}
                {/foreach}
            </span>
        {/if}

        {if isset($product.productmega)}
            {if isset($mega.extraAttrLong)}{$mega.extraAttrLong nofilter}{/if}
            <br/>
            <strong>{$mega.measure|escape:'htmlall':'UTF-8'}</strong>
            {if isset($mega.personalization) && $mega.personalization neq ''}
                <br/><div class="mp-personalization">{$mega.personalization nofilter}</div>
            {/if}
        {/if}

        {if $product.weight neq 0 and $CONFIGS.OPC_SHOW_WEIGHT}
            <span class="product_weight">
                <span>{l s='Weight' mod='onepagecheckoutps'}&nbsp;:&nbsp;</span>
                {$product.weight|string_format:"%.3f"|escape:'htmlall':'UTF-8'}{$PS_WEIGHT_UNIT}
            </span>
        {/if}

        {if $ps_stock_management and $CONFIGS.OPC_SHOW_AVAILABILITY}
            <div class="cart_avail">
                {strip}
                {*<span class="badge {if $product.availability == 'available'}badge-success product-available{elseif $product.availability == 'last_remaining_items'}badge-warning product-last-items{else}badge-danger product-unavailable{/if}">*}
                <span class="badge {if $product.quantity_available <= 0 || $product.cart_quantity > $product.quantity_available}badge-danger product-unavailable{else}badge-success product-available{/if}">
                    {if $product.quantity_available <= 0 || $product.cart_quantity > $product.quantity_available}
                        {if (isset($product.available_later) && $product.available_later) || (isset($PS_LABEL_OOS_PRODUCTS_BOA) && $PS_LABEL_OOS_PRODUCTS_BOA)}
                            {if isset($product.available_later) && $product.available_later}
                                {$product.available_later|escape:'htmlall':'UTF-8'}
                            {else}
                                {$PS_LABEL_OOS_PRODUCTS_BOA|escape:'htmlall':'UTF-8'}
                            {/if}
                        {else}
                            {$product.availability_message|escape:'htmlall':'UTF-8'}
                        {/if}
                    {else}
                        {if $product.quantity > $product.quantity_available}
                            {if isset($product.available_later) && $product.available_later}
                                {$product.available_later|escape:'htmlall':'UTF-8'}
                            {else}
                                {$product.availability_message|escape:'htmlall':'UTF-8'}
                            {/if}
                        {else}
                            {if isset($product.available_now) && $product.available_now}
                                {$product.available_now|escape:'htmlall':'UTF-8'}
                            {else}
                                {$product.availability_message|escape:'htmlall':'UTF-8'}
                            {/if}
                        {/if}
                    {/if}
                </span>
                {/strip}
                {if $CONFIGS.OPC_SHOW_DELIVERY_TIME && !empty($product.delivery_information_opc)}
                    <br/>
                    <span class="delivery-information">{$product.delivery_information_opc|escape:'htmlall':'UTF-8'}</span>
                {/if}
                {if !$product.is_virtual}{hook h="displayProductDeliveryTime" product=$product}{/if}
            </div>
        {/if}
        <div class="acciones">
            <span class="unidades">{$product.quantity|escape:'htmlall':'UTF-8'} {if $product.quantity > 1}unidades{else}unidad{/if}</span>
            <a
                class                       = "remove-from-cart"
                rel                         = "nofollow"
                href                        = "{$product.remove_from_cart_url|escape:'javascript'}{if isset($product.productmega)}&id_megacart={$mega.id_megacart|escape:'javascript'}{/if}"
                data-link-action            = "delete-from-cart"
                data-id-product             = "{$product.id_product|escape:'javascript'}"
                data-id-product-attribute   = "{$product.id_product_attribute|escape:'javascript'}"
                data-id-customization   	= "{$product.id_customization|escape:'javascript'}"
            >
                Eliminar
            </a>
        </div>
    </div>
    <div class="col-md-3 col-sm-2 col-xs-12 col-3 text-md-right text-xs-right text-sm-right text-right">
        <span class="total-price-text d-block d-md-none text-right">{l s='Total' mod='onepagecheckoutps'}</span>
        <span class="product-price">{if isset($product.productmega)}{$mega.stotalwt|escape:'htmlall':'UTF-8'}{else}{$product.total|escape:'htmlall':'UTF-8'}{/if}</span>
    </div>
</div>