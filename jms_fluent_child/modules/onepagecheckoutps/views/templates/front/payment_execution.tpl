{*
    * We offer the best and most useful modules PrestaShop and modifications for your online store.
    *
    * We are experts and professionals in PrestaShop
    *
    * @author    PresTeamShop.com <support@presteamshop.com>
    * @copyright 2011-2020 PresTeamShop
    * @license   see file: LICENSE.txt
    * @category  PrestaShop
    * @category  Module
*}

{foreach from=$payment_options item="option" key="name_module"}
    {$option->getAdditionalInformation() nofilter}
    {if $option->getForm()}
        {$option->getForm() nofilter}
        <div class="ps-shown-by-js">
            <button id="placer_order_payment" class="button btn-primary btn-block" type="button">
                {l s='Checkout' mod='onepagecheckoutps'}
            </button>
        </div>
    {else}
        <form id="payment-form" method="POST" action="{$option->getAction() nofilter}">
            {foreach from=$option->getInputs() item=input}
                <input type="{$input.type}" name="{$input.name}" value="{$input.value}">
            {/foreach}
            <div class="ps-shown-by-js">
                <button id="placer_order_payment" class="button btn-primary btn-block" type="button">
                    {l s='Checkout' mod='onepagecheckoutps'}
                </button>
            </div>
        </form>
    {/if}
{/foreach}