{*
    * We offer the best and most useful modules PrestaShop and modifications for your online store.
    *
    * We are experts and professionals in PrestaShop
    *
    * @author    PresTeamShop.com <support@presteamshop.com>
    * @copyright 2011-2020 PresTeamShop
    * @license   see file: LICENSE.txt
    * @category  PrestaShop
    * @category  Module
*}
<h6>Información</h6>
<div id="onepagecheckoutps_step_one_container" class="paso">
    <div id="onepagecheckoutps_step_one">
        {include file="./../address.tpl"}
    </div>
</div>