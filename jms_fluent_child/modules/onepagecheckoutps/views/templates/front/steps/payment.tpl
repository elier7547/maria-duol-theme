{*
    * We offer the best and most useful modules PrestaShop and modifications for your online store.
    *
    * We are experts and professionals in PrestaShop
    *
    * @author    PresTeamShop.com <support@presteamshop.com>
    * @copyright 2011-2020 PresTeamShop
    * @license   see file: LICENSE.txt
    * @category  PrestaShop
    * @category  Module
*}

{if !$register_customer}

    <h6>Pagos</h6>
    <div id="onepagecheckoutps_step_three_container" class="paso">
        <div id="bloque_pago" class="bloque">
            <div class="email_bloque linea">
                <div class="label_bloque">
                    Email:
                </div>
                <div class="contenido">
                
                </div>
                <div class="editar paso1">
                    Editar
                </div>
            </div>
            <div class="direccion_bloque linea">
                <div class="label_bloque">
                    Dirección:
                </div>
                <div class="contenido">
                
                </div>
                <div class="editar paso1">
                    Editar
                </div>
            </div>
            <div class="envio_bloque linea">
                <div class="label_bloque">
                    Envío:
                </div>
                <div class="contenido">
                    Envío gratuito (4-5 días)
                </div>
                <div class="editar paso2">
                    Editar
                </div>
            </div>
        </div>
        <h5 class="onepagecheckoutps_p_step onepagecheckoutps_p_step_three">
            <i class="fa-pts fa-pts-credit-card fa-pts-2x"></i>
            {l s='Payment method' mod='onepagecheckoutps'}
        </h5>
        <div id="onepagecheckoutps_step_three"></div>
    </div>
{/if}