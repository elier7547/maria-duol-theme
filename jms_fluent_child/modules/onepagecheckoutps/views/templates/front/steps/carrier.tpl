{*
    * We offer the best and most useful modules PrestaShop and modifications for your online store.
    *
    * We are experts and professionals in PrestaShop
    *
    * @author    PresTeamShop.com <support@presteamshop.com>
    * @copyright 2011-2020 PresTeamShop
    * @license   see file: LICENSE.txt
    * @category  PrestaShop
    * @category  Module
*}

{if !$register_customer}
    <h6>Envíos</h6>
    <div id="onepagecheckoutps_step_two_container" class="paso">
        <div id="bloque_envio" class="bloque">
            <div class="email_bloque linea">
                <div class="label_bloque">
                    Email:
                </div>
                <div class="contenido">
                
                </div>
                <div class="editar paso1">
                    Editar
                </div>
            </div>
            <div class="direccion_bloque linea">
                <div class="label_bloque">
                    Dirección:
                </div>
                <div class="contenido">
                
                </div>
                <div class="editar paso1">
                    Editar
                </div>
            </div>
        </div>
        <h5 class="onepagecheckoutps_p_step onepagecheckoutps_p_step_two">
            {l s='Shipping method' mod='onepagecheckoutps'}
        </h5>
        <div id="onepagecheckoutps_step_two"></div>
    </div>
{/if}