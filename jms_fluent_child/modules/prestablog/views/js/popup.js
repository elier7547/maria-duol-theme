/**
 * 2008 - 2020 (c) Prestablog
 *
 * MODULE PrestaBlog
 *
 * @author    Prestablog
 * @copyright Copyright (c) permanent, Prestablog
 * @license   Commercial
 * @version    4.3.5
 */


$(document).ready(function() {
	setTimeout(function() { $('.popup-content').modal('show'); }, $('.popup-content').data('delay') );
});
