{*
 * 2008 - 2020 (c) Prestablog
 *
 * MODULE PrestaBlog
 *
 * @author    Prestablog
 * @copyright Copyright (c) permanent, Prestablog
 * @license   Commercial
 * @version    4.3.5
 *}

<!-- Module Presta Blog -->
<div>
	<a title="{l s='Blog' mod='prestablog'}" href="{PrestaBlogUrl}">
		{l s='Blog' mod='prestablog'}
	</a>
</div>
<!-- /Module Presta Blog -->
