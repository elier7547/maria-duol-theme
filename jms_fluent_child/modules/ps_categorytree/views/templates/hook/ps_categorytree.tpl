{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{function name="categories" nodes=[] depth=0}
  {strip}
    {if $nodes|count}
		<div class="dropdown mostrardowncat" style="display: none">
			<a class="select-title" rel="nofollow" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<h3 style="float: left; font-size: 20px; cursor: pointer;">{l s='Categorías' d='Shop.Theme.CategoryTree'}</h3>
				<i class="fa fa-angle-down" style="margin-left: 10px"></i>
			</a>
			<div class="dropdown-menu">
                {foreach from=$nodes item=node}
					<li data-depth="{$depth}" class="cat-item" style="padding-left: 15px;">
                        {if $depth===0}
							{if $node.name!='Blog'}
							<a href="{$node.link}">
                                {$node.name}

							</a>
								{else}
								<a href="http://www.cosmeticaonco.com" target="_blank">
                                    {$node.name}

								</a>

								{/if}
                        {/if}
					</li>
                {/foreach}
			</div>
		</div>
      <ul class="category-sub-menu ocultarcat">

        {foreach from=$nodes item=node}
          <li data-depth="{$depth}" class="cat-item">
            {if $depth===0}
                {if $node.name!='Blog'}
					<a href="{$node.link}">
                        {$node.name}

					</a>
                {else}
					<a href="http://www.cosmeticaonco.com" target="_blank">
                        {$node.name}

					</a>

                {/if}

            {/if}
          </li>
        {/foreach}
      </ul>
    {/if}
  {/strip}
{/function}

<div class="block-categories hidden-sm-down marginbottomnone">
	{if $page.page_name != 'index'}
		<div class="title-block ocultarcat" >
			<h3>{l s='Categorías' d='Shop.Theme.CategoryTree'}</h3>
		</div>
	{/if}
		{categories nodes=$categories.children}
</div>
