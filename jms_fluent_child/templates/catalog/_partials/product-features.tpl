<div class="ocultartabs tab-pane fade  in{if $product.description} active{/if}"
     id="description"
     data-product="{$product.embedded_attributes|json_encode}"
     role="tabpanel"
  >


    {block name='product_description_short'}
        <div id="product-description-short-{$product.id}" class="product-desc"itemprop="description">{$product.description_short nofilter}</div>
    {/block}

</div>
