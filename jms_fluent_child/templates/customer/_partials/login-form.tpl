{include file='_partials/form-errors.tpl' errors=$errors['']}

{* TODO StarterTheme: HOOKS!!! *}

<form id="login-form" action="{$action}" method="post">

  <section>
    {block name='form_fields'}
      {foreach from=$formFields item="field"}
        {block name='form_field'}
          {form_field field=$field}
        {/block}
      {/foreach}
    {/block}

	<!--<div class="login_footer">
		<a href="{$urls.pages.password|escape:'htmlall':'UTF-8'}">{l s='�Olvidaste tu costrase�a?'}</a>
		<button type="button" id="btn_login" class="btn btn-info float-right">
			{l s='Iniciar sesi�n'}
        </button>
    </div>-->
	
    <!--<div class="forgot-password">-->
    <div class="login_footer">
      <a href="{$urls.pages.password}" rel="nofollow">
        <!--{l s='Forgot your password?' d='Shop.Theme.Customeraccount'}-->
        {l s='Forgot your password?' d='Shop.Theme.Customeraccount'}
      </a>
	  {*if isset($fb_on) && $fb_on}
		<br /><div class="fb-login-button" data-max-rows="1" data-size="{$JMSFB_BUTTON_SIZE}" data-button-type="{$JMSFB_BUTTON_TEXT}" data-show-faces="{if $JMSFB_SHOW_FRIENDS}true{else}false{/if}" data-auto-logout-link="{if $JMSFB_LOGOUT_BUTTON}true{else}false{/if}" data-use-continue-as="{if $JMSFB_PROFILE_INCLUDED}true{else}false{/if}"></div>
	  {/if*}
    <input type="hidden" name="submitLogin" value="1">
    {block name='form_buttons'}
      <button id="btn_login"  class="btn btn-default btn-effect btn-info float-right" data-link-action="sign-in" on-click=" {literal}

            dataLayer.push({
            {/literal}
                'event':'login'
                {literal}
            });

          {/literal}" type="submit" class="form-control-submit">
        {l s='Sign in' d='Shop.Theme.Actions'}


      </button>
    {/block}

    </div>	
  </section>

  <!--<footer class="form-footer text-xs-center clearfix">
    <input type="hidden" name="submitLogin" value="1">
    {block name='form_buttons'}
      <button id="btn_login"  class="btn btn-default btn-effect" data-link-action="sign-in" on-click=" {literal}

            dataLayer.push({
            {/literal}
                'event':'login'
                {literal}
            });

          {/literal}" type="submit" class="form-control-submit">
        {l s='Sign in' d='Shop.Theme.Actions'}


      </button>
    {/block}
  </footer>-->
</form>
