{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
 {$value = strpos($page.meta.title, 'sesi')}
 <div class="breadcrumb" {if $value == 8}style="padding-bottom: 25px;"{/if}>
<div class="breadcrumb-box container">
 

 {if $value == 8}
	<!--<span class="title_meta">-->
	<h5 class="titulo_iniciar"> {$page.meta.title}
		<span class="float-lg-right cuenta">
			&iquest;Todav&iacute;a no tienes cuenta con nosotras?
		<a id="mostrar_registro" href="{$urls.pages.register}" data-link-action="display-register-form">
	    {*<i class="fa-pts fa-pts-unlock-alt fa-pts-1x"></i>*}
		Reg&iacute;strate
		</a>
	    </h5>								
 {/if}
 {if $value == null}<span class="title_meta" style="display: none">{$page.meta.title}</span>{/if}
 <!--<p>{$page.meta.title}</p>
 <p>{$value}</p>
<span class="title_meta" style="display: none">{$page.meta.title}</span>
<span class="title_meta" style="padding-bottom: 25px;">{$page.meta.title}</span>-->
	<div data-depth="{$breadcrumb.count}" class="breadcrumb-inner hidden-sm-down">
  <!--<ol itemscope itemtype="http://schema.org/BreadcrumbList">-->
  <ol itemscope itemtype="http://schema.org/BreadcrumbList" {if $value == 8}style="display: none"{/if}>
    {foreach from=$breadcrumb.links item=path name=breadcrumb}
      <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="{$path.url}">
           <span itemprop="name">{$path.title}</span>
        </a>
        <meta itemprop="position" content="{$smarty.foreach.breadcrumb.iteration}">
      </li>
    {/foreach}
  </ol>
</div>
</div>
</div>
