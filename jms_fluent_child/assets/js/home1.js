
$(window).load(function() {
	// INICIALIZO LOS DOS SLIDERS POR SEPARADO PORQUE SINO SE PISAN
	// EL MÓDULO NO ESTÁ PENSADO AL 100% PARA TENER 2 EN LA MISMA PAG
	if($('.altimagen .slider').length > 0) {
		$('.altimagen .slider').fractionSlider({	
			'slideTransition' : jmsslider_trans,
			'slideEndAnimation' : jmsslider_end_animate,
			'transitionIn' : jmsslider_trans_in,
			'transitionOut' : jmsslider_trans_out,
			'fullWidth' : jmsslider_full_width,
			'delay' : jmsslider_delay,
			'timeout' : jmsslider_duration,
			'speedIn' : jmsslider_speed_in,
			'speedOut' : jmsslider_speed_out,
			'easeIn' : jmsslider_ease_in,
			'easeOut' : jmsslider_ease_out,
			'controls' : jmsslider_navigation,
			'pager' : jmsslider_pagination,
			'autoChange' : jmsslider_autoplay,
			'pauseOnHover' : jmsslider_pausehover,
			'backgroundAnimation' : jmsslider_bg_animate,
			'backgroundEase' : jmsslider_bg_ease,
			'responsive' : jmsslider_responsive,
			'dimensions' : jmsslider_dimensions,
			'fullscreen' : true
		});
	}

	if($('.slider_home2 .slider').length > 0) {
		$('.slider_home2 .slider').fractionSlider({	
			'slideTransition' : jmsslider_trans,
			'slideEndAnimation' : jmsslider_end_animate,
			'transitionIn' : jmsslider_trans_in,
			'transitionOut' : jmsslider_trans_out,
			'fullWidth' : jmsslider_full_width,
			'delay' : jmsslider_delay,
			'timeout' : jmsslider_duration,
			'speedIn' : jmsslider_speed_in,
			'speedOut' : jmsslider_speed_out,
			'easeIn' : jmsslider_ease_in,
			'easeOut' : jmsslider_ease_out,
			'controls' : jmsslider_navigation,
			'pager' : jmsslider_pagination,
			// 'autoChange' : jmsslider_autoplay,
			'autoChange' : true,
			'pauseOnHover' : jmsslider_pausehover,
			'backgroundAnimation' : jmsslider_bg_animate,
			'backgroundEase' : jmsslider_bg_ease,
			'responsive' : jmsslider_responsive,
			'dimensions' : jmsslider_dimensions,
			'fullscreen' : true
		});
	}
});
jQuery(function ($) {
    "use strict";
	if($(".producttab-carousel").length) {
			var producttabCarousel = $(".producttab-carousel");			
			var rtl = false;
			if ($("body").hasClass("rtl")) rtl = true;				
			producttabCarousel.owlCarousel({
				responsiveClass:true,
				responsive:{			
					1199:{
						items:tab_itemsDesktop
					},
					991:{
						items:tab_itemsDesktopSmall
					},
					768:{
						items:tab_itemsTablet
					},
					481:{
						items:tab_itemsMobile
					},
					361:{
						items:1
					},
					0: {
						items:1
					}
				},
				rtl: rtl,
					margin: 30,
				    nav: p_nav_tab,
			        dots: p_pag_tab,
					autoplay: auto_play_tab,
					loop:true,
				    navigationText: ["", ""],
				    slideSpeed: 200
			});
		}
 	
});
