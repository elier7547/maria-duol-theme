
[{shop_url}] 

Hola {firstname} {lastname},

Gracias por comprar con {shop_name}!

Su pago por pedido con la referencia {order_name} fue
procesado con &eacute;xito.

Puede revisar su pedido y descargar su factura desde
"Historial de pedidos" [{history_url}] secci&oacute;n de tu
cuenta de cliente haciendo clic en "Mi cuenta"
[{my_account_url}] en nuestra tienda.

Si tiene una cuenta de invitado, puede seguir su pedido a trav&eacute;s de
"Seguimiento de invitados"
[{guest_tracking_url}?id_order={order_name}]
secci&oacute;n en nuestra tienda.

{shop_name}(tm) [{shop_url}]

